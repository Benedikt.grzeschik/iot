#!/usr/bin/python3
import can
import time
import json
from websocket_server import WebsocketServer

# define function for greeting new client
def greet(client, server):
    print('Hello new client')

# create CAN Bus amd Server
print('Setting up CAN')
bus = can.Bus(channel='can1', interface='socketcan')
print('Setting up Server')
server = WebsocketServer(host='192.168.150.1', port=6060)
# set greeting function for new clients connecting
server.set_fn_new_client(greet)
# set server to running
server.run_forever(True)

# receive all the time messages
while (True):
    print('Waiting for Message')
    # receive message
    message = bus.recv()
    print(message)
    print(str(message.data[0]) + ' ' + str(message.data[1]))
    # generate object for sending to frontend
    payload = {
        'temperature': message.data[0],
        'humidity': message.data[1]
    }
    json_payload = json.dumps(payload, indent=4)
    # send payload to frontend client
    print('Send Message')
    server.send_message_to_all(json_payload)
