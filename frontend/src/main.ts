import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { DataService } from './app/services/data.service';

DataService.Init();

bootstrapApplication(AppComponent, appConfig).catch((err) =>
  console.error(err)
);
