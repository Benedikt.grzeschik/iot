import { Component, Input, OnInit, } from '@angular/core';
import { DataService } from '../services/data.service';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-temperature-widget',
  templateUrl: './temperature-widget.component.html',
  styleUrls: ['./temperature-widget.component.css'],
  standalone: true,
})
export class TemperatureWidgetComponent {
  @Input() temperature: number = 23;
  innerClass: string = 'low';

  ngOnInit() {
    DataService.temperatureChange.subscribe((value) => {
      this.temperature = value;
      this.updateInnerClass();
    });
  }

  updateInnerClass() {
    if (this.temperature < 10) {
      this.innerClass = 'low';
    } else if (this.temperature < 20) {
      this.innerClass = 'medium';
    } else {
      this.innerClass = 'high';
    }
  }

  constructor() {}
}
