import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private static socket: WebSocket;

  public static temperatureChange: Subject<number> = new Subject<number>();
  public static humidityChange: Subject<number> = new Subject<number>();

  constructor() {}

  public static Init() {
    console.log('Initialize the websocket');

    DataService.socket = new WebSocket('ws://192.168.150.1:6060');

    DataService.socket.onopen = function () {
      console.log('Connection is established');
    };

    DataService.socket.onmessage = function (event: any) {
      console.log(`Message received ${event.data}`);
      var data = JSON.parse(event.data);
      DataService.temperatureChange.next(data.temperature);
      DataService.humidityChange.next(data.humidity);
    };
  }

  public static getTemperatureUpdates(): Observable<number> {
    return DataService.temperatureChange.asObservable();
  }

  public static getHumidityUpdates(): Observable<number> {
    return DataService.humidityChange.asObservable();
  }
}