import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from './services/data.service';

import { TemperatureWidgetComponent } from './temperature-widget/temperature-widget.component';
import { HumidityWidgetComponent } from './humidity-widget/humidity-widget.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  standalone: true,
  imports: [TemperatureWidgetComponent, HumidityWidgetComponent]
})
export class AppComponent implements OnInit, OnDestroy {
  temperature: number = 0; // Default to 0
  humidity: number = 0; // Default to 0
  private temperatureSubscription: Subscription = new Subscription(); // Initialized with a new Subscription
  private humiditySubscription: Subscription = new Subscription(); // Initialized with a new Subscription
  
  constructor(private DataService: DataService) {}

  ngOnInit(): void {
    DataService.Init();
    // Subscribe to temperature updates
    this.temperatureSubscription = DataService.getTemperatureUpdates().subscribe(data => {
      this.temperature = data;
    });

    // Subscribe to humidity updates
    this.humiditySubscription = DataService.getHumidityUpdates().subscribe(data => {
      this.humidity = data;
    });
  }

  ngOnDestroy(): void {
    // Unsubscribe to prevent memory leaks
    if (this.temperatureSubscription) {
      this.temperatureSubscription.unsubscribe();
    }
    if (this.humiditySubscription) {
      this.humiditySubscription.unsubscribe();
    }
  }
}