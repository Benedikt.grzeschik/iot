import { Component, Input } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-humidity-widget',
  templateUrl: './humidity-widget.component.html',
  styleUrls: ['./humidity-widget.component.css'],
  standalone: true,
})
export class HumidityWidgetComponent {
  @Input() humidity: number = 0; // Bind this to your humidity data
  constructor() {
    DataService.humidityChange.subscribe((value) => {
      console.log(`Change humidity to ${value}`);
      this.humidity = value;
    });
  }
}
